//Soal 1
console.log("-------------- Soal 1 --------------");

// var nilai = 69;

// if (nilai >= 85 && nilai <= 100) {
//   console.log("Indeksnya A");
// } else if (nilai >= 75 && nilai < 85) {
//   console.log("Indeksnya B");
// } else if (nilai >= 65 && nilai < 75) {
//   console.log("Indeksnya C");
// } else if (nilai >= 55 && nilai < 65) {
//   console.log("Indeksnya D");
// } else if (nilai >= 0 && nilai < 55) {
//   console.log("Indeksnya E");
// } else {
//   console.log("Indeksnya tidak tersedia");
// }

var nilai;
var index;

nilai = 88;
if (nilai >= 85 && nilai <= 100) {
  index = "A";
} else if (nilai >= 75 && nilai < 85) {
  index = "B";
} else if (nilai >= 65 && nilai < 75) {
  index = "C";
} else if (nilai >= 55 && nilai < 65) {
  index = "D";
} else {
  index = "E";
}
console.log("Indeksnya " + index);

//Soal 2
console.log("-------------- Soal 2 --------------");

var tanggal = 15;
var bulan = 8;
var tahun = 1986;

var bulan_str;
switch (bulan) {
  case 1:
    bulan_str = "Januari";
    break;
  case 2:
    bulan_str = "Februari";
    break;
  case 3:
    bulan_str = "Maret";
    break;
  case 4:
    bulan_str = "April";
    break;
  case 5:
    bulan_str = "Mei";
    break;
  case 6:
    bulan_str = "Juni";
    break;
  case 7:
    bulan_str = "Juli";
    break;
  case 8:
    bulan_str = "Agustus";
    break;
  case 9:
    bulan_str = "September";
    break;
  case 10:
    bulan_str = "Oktober";
    break;
  case 11:
    bulan_str = "November";
    break;
  case 12:
    bulan_str = "Desember";
    break;
}
console.log(tanggal, bulan_str, tahun);

//Soal 3
console.log("-------------- Soal 3 --------------");

var n = 3;
var output = "";
for (var i = 0; i < n; i++) {
  for (var j = 0; j <= i; j++) {
    output += "#";
  }
  output += "\n";
}
console.log(output);

// var n = 7;
// for (var i = 1; i < n + 1; i++) {
//   var output = "";
//   for (var j = 0; j < i; j++) {
//     output += "#";
//   }
//   console.log(output);
// }

//Soal 4
console.log("-------------- Soal 4 --------------");

// var m = 10;
// var output = "";

// for (var i = 1; i <= m; i++) {
//   if (i % 3 == 0) {
//     console.log(i + " - I Love VueJS");
//     output += "===";
//     console.log(output);
//   } else if ((i % 3) - 1 == 0) {
//     console.log(i + " - I Love programming");
//   } else {
//     console.log(i + " - I Love Javascript");
//   }
// }

var m = 10;
for (var i = 1; i <= m; i++) {
  j = i % 3;
  switch (j) {
    case 1:
      console.log(i + " - I Love programming");
      break;
    case 2:
      console.log(i + " - I Love Javascript");
      break;
    default:
      console.log(i + " - I Love VueJS");
      var sama_dengan = "";
      for (var j = 0; j < i; j++) {
        sama_dengan += "=";
      }
      console.log(sama_dengan);
  }
}

// console.log("LOOPING PERTAMA");
// var i = 2;
// while (i <= 20) {
//   console.log(i + " - I Love Coding");
//   i += 2;
// }
// console.log("LOOPING KEDUA");
// var i = 20;
// while (i > 0) {
//   console.log(i + " - I will become a frontend developer");
//   i -= 2;
// }

// console.log("LOOPING PERTAMA");
// for (var i = 1; i <= 20; i++) {
//   if (i % 2 == 0) {
//     console.log(i + " - I Love Coding");
//   }
// }
// console.log("LOOPING KEDUA");
// for (var i = 20; i > 0; i--) {
//   if (i % 2 == 0) {
//     console.log(i + " - I will become a frontend developer");
//   }
// }

// for (var i = 1; i <= 20; i++) {
//   if (i % 2 == 1) {
//     if (i % 3 == 0) {
//       console.log(i + " - I Love Coding");
//     } else {
//       console.log(i + " - Santai");
//     }
//   } else {
//     console.log(i + " - Berkualitas");
//   }
// }
