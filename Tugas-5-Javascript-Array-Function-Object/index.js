//Soal 1
console.log("-------------- Soal 1 --------------");

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
daftarHewan.forEach(function (item) {
  console.log(item);
});

// var daftarHewanSorted = daftarHewan.sort();
// for (var index = 0; index < daftarHewanSorted.length; index++) {
//   console.log(daftarHewanSorted[index]);
// }

//Soal 2
console.log("-------------- Soal 2 --------------");

function introduce(dt) {
  return (
    "Nama saya " +
    dt.name +
    ", umur saya " +
    dt.age +
    " tahun, alamat saya di " +
    dt.address +
    ", dan saya punya hobby yaitu " +
    dt.hobby +
    "."
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

//Soal 3
console.log("-------------- Soal 3 --------------");

function hitung_huruf_vokal(str) {
  var output = 0;
  var vokal = "AaIiUuEeOo";
  for (var i = 0; i < str.length; i++) {
    if (vokal.indexOf(str[i]) !== -1) {
      output += 1;
    }
  }
  return output;
}

// function hitung_huruf_vokal(str) {
//   var output = 0;
//   var vokal = "aiueo";
//   for (var i = 0; i < str.length; i++) {
//     if (vokal.indexOf(str.toLowerCase()[i]) !== -1) {
//       output += 1;
//     }
//   }
//   return output;
// }

// function hitung_huruf_vokal(str) {
//   var output = 0;
//   for (var i = 0; i < str.length; i++) {
//     if (
//       str[i] == "a" ||
//       str[i] == "i" ||
//       str[i] == "u" ||
//       str[i] == "e" ||
//       str[i] == "o" ||
//       str[i] == "A" ||
//       str[i] == "I" ||
//       str[i] == "U" ||
//       str[i] == "E" ||
//       str[i] == "O"
//     ) {
//       output += 1;
//     }
//   }
//   return output;
// }

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

//Soal 4
console.log("-------------- Soal 4 --------------");

function hitung(num) {
  return num * 2 - 2;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
