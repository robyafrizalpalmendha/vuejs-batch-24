export const MemberComponent = {
  props: ["members"],
  template: `
    <div>
        <table border="1">
            <tr v-for="member of members">
                <td>
                    <img width="200" :src="member.photo_profile ? 'http://demo-api-vue.sanbercloud.com' + member.photo_profile : 'https://dummyimage.com/16:9x1080'" alt="">
                </td>
                <td>
                    <b>Name :</b> {{member.name}} <br>
                    <b>Address :</b> {{member.address}} <br>
                    <b>Phone :</b> {{member.no_hp}} <br>
                </td>
                <td>
                    <button @click="$emit('edit',member)">Edit</button>
                    <button @click="$emit('delete',member.id)">Hapus</button>
                    <button @click="$emit('upload',member)">Upload Photo</button>
                </td>
            </tr>
        </table>
    </div>
    `,
};
