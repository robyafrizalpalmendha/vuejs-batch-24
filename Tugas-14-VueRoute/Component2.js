export const Component2 = {
  data() {
    return {
      activities: [
        {
          id: 1,
          name: "Bangun Tidur",
        },
        {
          id: 2,
          name: "Mandi",
        },
        {
          id: 3,
          name: "Kerja",
        },
      ],
    };
  },
  template: `
    <div>
      <h1>Routine Activity</h1>
      <ul>
        <li v-for="activity of activities">{{activity.id}}. {{activity.name}}</li>
      </ul>
    </div>`,
};
