var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var inputWaktu = 9000;
readBooksPromise(inputWaktu, books[0])
  .then((sisa) => {
    //   console.log(sisa)
    readBooksPromise(sisa, books[1]).then((sisa) => {
      //   console.log(sisa);
      readBooksPromise(sisa, books[2]).then((sisa) => console.log(sisa));
    });
  })
  .catch((err) => console.log(err));

//Async Await
// const init = async () => {
//   const resp1 = await readBooksPromise(inputWaktu, books[0]);
//   const resp2 = await readBooksPromise(resp1, books[1]);
//   const resp3 = await readBooksPromise(resp2, books[2]);
//   console.log(resp3);
// };
// init();
