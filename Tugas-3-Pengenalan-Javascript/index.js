//Soal 1
console.log("-------------- Soal 1 --------------");

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var kata1 = pertama[0] + pertama[1] + pertama[2] + pertama[3];
var kata2 =
  pertama[12] +
  pertama[13] +
  pertama[14] +
  pertama[15] +
  pertama[16] +
  pertama[17];
var kata3 =
  kedua[0] + kedua[1] + kedua[2] + kedua[3] + kedua[4] + kedua[5] + kedua[6];
var kata4 =
  kedua[8] +
  kedua[9] +
  kedua[10] +
  kedua[11] +
  kedua[12] +
  kedua[13] +
  kedua[14] +
  kedua[15] +
  kedua[16] +
  kedua[17];

var output = kata1 + " " + kata2 + " " + kata3 + " " + kata4.toUpperCase();
console.log(output);

//Soal 2
console.log("-------------- Soal 2 --------------");

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var output =
  Number(kataPertama) +
  Number(kataKedua) * Number(kataKetiga) +
  Number(kataKeempat);
console.log(output);

//Soal 3
console.log("-------------- Soal 3 --------------");

var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
