//Judul : Function Penghitung Jumlah Kata

function jumlah_kata(str) {
  var output = 1;
  for (var i = 0; i <= str.length; i++) {
    if (str.substring(i, i + 1) == " ") {
      output++;
    }
  }
  console.log(output);
}

// function jumlah_kata(str) {
//   var word = str.trim().split(" ");
//   console.log(word.length);
// }

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2

//Judul : Function Penghasil Tanggal Hari Esok
console.log("---------------------------------");

function next_date(date, month, year) {
  if (date == 29 && month == 2) {
    date = 1;
    month = "Maret";
  } else if (date == 31 && month == 12 && year == 2020) {
    date = 1;
    month = "Januari";
    year = year + 1;
  } else if (date == 31) {
    date = 1;
    switch (month + 1) {
      case 1:
        month = "Januari";
        break;
      case 2:
        month = "Februari";
        break;
      case 3:
        month = "Maret";
        break;
      case 4:
        month = "April";
        break;
      case 5:
        month = "Mei";
        break;
      case 6:
        month = "Juni";
        break;
      case 7:
        month = "Juli";
        break;
      case 8:
        month = "Agustus";
        break;
      case 9:
        month = "September";
        break;
      case 10:
        month = "Oktober";
        break;
      case 11:
        month = "November";
        break;
      case 12:
        month = "Desember";
        break;
    }
  } else {
    date = date + 1;
    switch (month) {
      case 1:
        month = "Januari";
        break;
      case 2:
        month = "Februari";
        break;
      case 3:
        month = "Maret";
        break;
      case 4:
        month = "April";
        break;
      case 5:
        month = "Mei";
        break;
      case 6:
        month = "Juni";
        break;
      case 7:
        month = "Juli";
        break;
      case 8:
        month = "Agustus";
        break;
      case 9:
        month = "September";
        break;
      case 10:
        month = "Oktober";
        break;
      case 11:
        month = "November";
        break;
      case 12:
        month = "Desember";
        break;
    }
  }
  console.log(date + " " + month + " " + year);
}

var tanggal = 29;
var bulan = 2;
var tahun = 2020;

next_date(tanggal, bulan, tahun); // output : 1 Maret 2020
