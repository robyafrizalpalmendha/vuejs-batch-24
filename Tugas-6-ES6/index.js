//Soal 1
console.log("-------------- Soal 1 --------------");
// let p;
// let l;
// const luasPersegiPanjang = (p, l) => p * l;
// const kelillingPersegiPanjang = (p, l) => 2 * (p + l);

const luasPersegiPanjang = (panjang, lebar) => {
  const luas = panjang * lebar;
  return luas;
};
const kelillingPersegiPanjang = (panjang, lebar) => {
  const keliling = 2 * (panjang + lebar);
  return keliling;
};

console.log(luasPersegiPanjang(5, 2));
console.log(kelillingPersegiPanjang(5, 2));

//Soal 2
console.log("-------------- Soal 2 --------------");

const newFunction = (literal = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
});

newFunction("William", "Imoh").fullName();

//Soal 3
console.log("-------------- Soal 3 --------------");

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

//Soal 4
console.log("-------------- Soal 4 --------------");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];

console.log(combined);

//Soal 5
console.log("-------------- Soal 5 --------------");

const planet = "earth";
const view = "glass";

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(before);
